package com.example.administrasipublikunpad.ui.capaianpembelajaran

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.administrasipublikunpad.R
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.util.FitPolicy
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener
import kotlinx.android.synthetic.main.capaian_pembelajaran_fragment.*

class CapaianPembelajaranFragment : Fragment(), OnPageChangeListener {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.capaian_pembelajaran_fragment, container, false)
        val pdfPageNumber: TextView = root.findViewById(R.id.text_nomor_halaman)
        pdfPageNumber.text = getString(R.string.nomor_halaman, 1)
        val pdfView: PDFView = root.findViewById(R.id.pdf_capaian_pembelajaran)
        pdfView.fromAsset("pdf/capaian_pembelajaran.pdf")
                .swipeHorizontal(true)
                .pageSnap(true)
                .autoSpacing(true)
                .pageFling(true)
                .onPageChange(this)
                .pageFitPolicy(FitPolicy.BOTH)
                .enableAntialiasing(true)
                .load()
        return root
    }

    override fun onPageChanged(page: Int, pageCount: Int) {
        val pageNumber = page + 1
        text_nomor_halaman.text = getString(R.string.nomor_halaman, pageNumber)
    }
}