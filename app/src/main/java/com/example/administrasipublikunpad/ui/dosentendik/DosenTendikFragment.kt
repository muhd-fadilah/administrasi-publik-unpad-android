package com.example.administrasipublikunpad.ui.dosentendik

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import com.example.administrasipublikunpad.R

class DosenTendikFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.dosen_tendik_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_dosen_tendik)
        webView.loadUrl("file:///android_asset/html/dosen_tendik.html")
        return root
    }
}