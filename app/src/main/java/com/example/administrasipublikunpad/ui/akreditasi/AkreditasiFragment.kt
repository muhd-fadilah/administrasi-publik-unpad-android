package com.example.administrasipublikunpad.ui.akreditasi

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import com.example.administrasipublikunpad.R

class AkreditasiFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.akreditasi_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_akreditasi)
        webView.loadUrl("file:///android_asset/html/akreditasi.html")
        return root
    }
}