package com.example.administrasipublikunpad.ui.sejarah

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.TextView
import androidx.lifecycle.Observer
import com.example.administrasipublikunpad.R

class SejarahFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.sejarah_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_sejarah)
        webView.loadUrl("file:///android_asset/html/sejarah.html")
        return root
    }
}