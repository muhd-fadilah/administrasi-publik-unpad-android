package com.example.administrasipublikunpad.ui.prospekkerja

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.widget.TextView
import androidx.lifecycle.Observer
import com.example.administrasipublikunpad.R
import com.example.administrasipublikunpad.ui.profillulusan.ProfilLulusanFragment

class ProspekKerjaFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.prospek_kerja_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_prospek_kerja)
        webView.loadUrl("file:///android_asset/html/prospek_kerja.html")
        return root
    }
}