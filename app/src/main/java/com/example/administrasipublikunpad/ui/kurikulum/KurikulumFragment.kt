package com.example.administrasipublikunpad.ui.kurikulum

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import com.example.administrasipublikunpad.R

class KurikulumFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.kurikulum_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_kurikulum)
        webView.loadUrl("file:///android_asset/html/kurikulum.html")
        return root
    }
}