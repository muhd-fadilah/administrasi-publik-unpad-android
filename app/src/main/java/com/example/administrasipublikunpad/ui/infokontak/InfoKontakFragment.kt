package com.example.administrasipublikunpad.ui.infokontak

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import com.example.administrasipublikunpad.R

class InfoKontakFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.info_kontak_fragment, container, false)
        val webView: WebView = root.findViewById(R.id.web_info_kontak)
        webView.loadUrl("file:///android_asset/html/info_kontak.html")
        return root
    }
}